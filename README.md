# WASM Virtual Machine

## Installation
1. Install WABT: https://github.com/webassembly/wabt

## Running
1. Compile sample WASM s-expression:
```bash
wat2wasm sample.wat
```
